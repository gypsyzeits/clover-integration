<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              LeadplanMarketing.com
 * @since             1.0.0
 * @package           Clover_Integration
 *
 * @wordpress-plugin
 * Plugin Name:       Clover Integration
 * Plugin URI:        LeadplanMarketing.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            James Zeits
 * Author URI:        LeadplanMarketing.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       clover-integration
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function update_clover_products (){
	ob_end_flush();
	global $wpdb;
	//determine update csv
	//this so far looks like a static file
	//open blacklist file
	$blacklist = array();
	if ($handle = fopen(plugin_dir_path(__file__ ).'blacklist.txt','r')){
		while ($data = fgets($handle)){
			$blacklist[]= strtolower(trim($data));
		}
		fclose($handle);
		
	}

	//open import csv
	$header = null;
	$products = array();
	if ($handle = fopen(plugin_dir_path(__file__ ).'/imports/CIG_export_E7641_CUST-143363.csv', 'r')){
		while( $data = fgetcsv($handle, 10000,';','"') ){
			//keep track of product categories
			$product_categories = array();
			
			//setup the array header 
			if (!$header){
				$header = $data;
				foreach ($header as $key => $value) {
					$header[$key] = str_replace(' ','',$value);
				}
			}
			//actually start importing stuff
			else{
				$product = array_combine($header, $data);
				// if OEM is not in the black list and is not empty 
				if (!in_array(strtolower($product['OEM']),$blacklist) and $product['OEM'] !== ''){
					clover_update_product( $product, $product_categories );
					set_time_limit(30);
					flush();
				}

			}

		}
		//var_dump($blacklist);
		//var_dump($products);
		fclose($handle);
	}

	//var_dump($products[0]);	
	//update/import product 
	//check to see if product exists



	//wp_die( $message, $title, $args );
}
function clover_integration_settings_page(){
	include_once('settings-page.php');
}
function add_clover_integration_admin_settings (){
	add_options_page( 'Clover Integration Settings', 'Clover Integration', 'manage_options', 'clover_integration', 'clover_integration_settings_page' );
}

function clover_update_product($product, &$categories){
	

	if (!$product){
		return false;
	}
	if ( strpos(strtolower($product['Title']), 'non-oem') !== FALSE){
		return false;
	}

	$post = get_page_by_title( $product['Title'], OBJECT, 'product' );
	
	//echo $product['Title'].' ...starting'.'<br>';
	//set variables that should always be updated
		


	//check metadata for existing sku = upc
	//var_dump($product);
	// $meta_query_args = array(
	// 	'meta_key' 		=>	'_sku',
	// 	'meta_value'	=>	$product['Item#'],
	// 	'post_type'		=> 	'product'
	// 	);

	// $the_query = new WP_Query($meta_query_args);
	
	$post_content = $product['ShortDescription'];
	$post_content .= '<br>OEM Part Number: '.$product['OEMPart#'];	
	$post_content .= '<br>Clover Part Number: '.$product['Item#'];	
	$post_content .= '<br>UPC Code: '.$product['UPCCode'];	
	$post_content .= '<br>Type: '.$product['Type'];	
	$post_content .= '<br>Color: '.$product['Color'];	
	$post_content .= '<br>Brand: '.$product['Brand'];	
	$post_content .= '<br>Comparable OEM\'s: '.$product['ComparableOEMs'];
	$post_content .= '<br><br>Long Description: '.$product['LongDescription'];
	$post_content .= '<br><br>Compatibility: '.$product['Compatibility'];
	$post_content .= '<br><br>Search Keywords: '.$product['SearchKeywords'];

	//setup post and metadata arrays

	$post_args = array(
		'post_content'	=>	str_replace('CIG', 'NWOE',$post_content),
		'post_title'	=>	str_replace('CIG', 'NWOE', $product['Title']),
		'post_status'	=>	'publish',
		'post_type'		=>	'product'
		);

	//var_dump($the_query->have_posts());
	
	
	//check if the post exists
	if ($post){

		//get the post id and add it to the args
		
		$post_args['ID'] = $post->ID;
		
		//var_dump($the_query->post->ID);
	
		$post_id = wp_insert_post($post_args);
		// foreach ($post_meta_args as $key => $value) {
		// 	update_post_meta( $post_id, $key, $value );
		// }

	}

	else{
		$post_id = wp_insert_post($post_args);

		$post_meta_args = array(
			'_cig_product'				=> 'Yes',
			'_total_sales'				=> '0',
			'_visibility'				=>'visible',
			'_stock_status' 			=>'instock',
			'_downloadable' 			=>'no',
			'_virtual' 					=>'no',
			'_purchase_note' 			=>'',
			'_featured' 				=>'no',
			'_weight' 					=>$product['Weight'],
			'_length' 					=>$product['Length'],
			'_width' 					=>$product['Width'],
			'_height' 					=>$product['Height'],
			'_sku' 						=>$product['Item#'],
			'_product_attributes' 		=>array(),
			'_sale_price_dates_from' 	=>'',
			'_sale_price_dates_to' 		=>'',
			'_sold_individually' 		=>'',
			'_manage_stock' 			=>'no',
			'_backorders' 				=>'no',
			'_stock' 					=>'',
			'_UPCCode' 					=>$product['UPCCode'],
			'_OEMPart#'					=>$product['OEMPart#'],
			);
		
		foreach ($post_meta_args as $key => $value) {
					update_post_meta( $post_id, $key, $value );
				}
		
		wp_set_object_terms( $post_id, 'simple', 'product_type' );

		$pricingData=curl_clover_price_request($product['Item#']);
		
		$price = markup_clover_price($pricingData->Price);
		
		//var_dump($pricingData);
		//var_dump($price);
		
		update_post_meta( $post_id, '_regular_price', $price );
		update_post_meta( $post_id, '_price', $price );

		//echo $post_id.' ' .$product['Title'].' ...set categories'.'<br>';

		//set product categories
		$searchKeywords = explode(', ', $product['SearchKeywords']);
		
		//$searchKeywords = array_filter($searchKeywords, "remove_product_numbers");
		//$searchKeywords = str_replace(' ','-',$searchKeywords);
		//$searchKeywords = array_map('strtolower', $searchKeywords);


		//Categories White List
		$CatWhiteList = Array(
			'Ribbon',
			'Laser Toner Cartridge',
			'Inkjet Cartridge',
			'Drum Unit'
			);

		$categories = array_intersect($CatWhiteList, $searchKeywords);
		$categories = str_replace(' ','-',$categories);
		$categories = array_map('strtolower', $categories);

		if ( strpos($post_args['post_title'], 'NWOE') !== FALSE ){
			wp_set_object_terms( $post_id, 'nwoe', 'product_cat', true );
		}

		if ( strpos( $post_args['post_title'], 'MSE') !== FALSE ){
			wp_set_object_terms( $post_id, 'mse', 'product_cat', true );	
		}

		wp_set_object_terms( $post_id, $categories, 'product_cat', true );		
		wp_set_object_terms( $post_id, 'drums-toner-ink-cartridges', 'product_cat', true );
		wp_set_object_terms( $post_id, $product['OEM'], 'product_cat', true );
		var_dump($categories);
	}

	

	//echo $post_id.' ' .$product['Title'].' getting image'.'<br>';

	//get and set image if not already set
	if ( !get_post_meta($post_id, '_thumbnail_id', true) && $product['ImageURL'] ){
		$imageID = media_sideload_image( $product['ImageURL'], $post_id, $product['Title'], 'id' );
		update_post_meta($post_id, '_thumbnail_id', $imageID);
		//var_dump($imageID);
	}

	//echo $post_id.' ' .$product['Title'].' ...complete'.'<br>';
	//wp_die();
}
function remove_product_numbers($word){
	if ( preg_match('~[0-9]~', $word) === 1){
		return false;
	}
	return true;
}

function curl_clover_price_request($sku){

	$xml = "<?xml version=\"1.0\"?>
	<PriceRequest>
		<Credential>
			<UserID>CUST-140589</UserID>
			<CompanyCd>01</CompanyCd>
			<SenderID>northwoodsprinters</SenderID>
			<Password>!northwoodsprinters!</Password>
		</Credential>
		<ItemList>
			<SKU>$sku</SKU>
		</ItemList>
	</PriceRequest>";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,'http://www.westpointproducts.com/ci/CustomerIntegration.aspx');
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	$response = curl_exec($ch);
	$data = json_decode(json_encode(simplexml_load_string($response)));
	return $data->PriceAvailabilityList;
}

function markup_clover_price($price){
	$price = floatval($price) * 1.75;
	$price = round($price, 0, PHP_ROUND_HALF_UP) - .01;
	return $price;
}

add_action('admin_post_clover_integration_import', 'update_clover_products');
add_action('admin_menu', 'add_clover_integration_admin_settings');
add_action('scheduled_clover_update', 'update_clover_products');

register_activation_hook(__FILE__, 'my_activation');
function my_activation() {
    if (! wp_next_scheduled ( 'scheduled_clover_update' )) {
	wp_schedule_event(strtotime('today midnight'), 'daily', 'my_hourly_event');
    }
}

register_deactivation_hook(__FILE__, 'my_deactivation');
function my_deactivation() {
	wp_clear_scheduled_hook('scheduled_clover_update');
}




/**
 * Add a custom action to order actions select box on edit order page
 * Only added for paid orders that haven't fired this action yet
 *
 * @param array $actions order actions array to display
 * @return array - updated actions
 */
function clover_add_action_to_wc_metabox( $actions ) {
    global $theorder;

    // bail if the order has been paid for or this action has been run
    if ( ! $theorder->is_paid() || get_post_meta( $theorder->id, '_wc_order_clover_drop_shipped', true ) ) {
        return $actions;
    }

    // add "mark printed" custom action
    $actions['wc_clover_drop_ship'] = __( 'Drop Ship All Available Products Via Clover', 'my-textdomain' );
    return $actions;
}
add_action( 'woocommerce_order_actions', 'clover_add_action_to_wc_metabox' );

/**
 * Add an order note when custom action is clicked
 * Add a flag on the order to show it's been run
 *
 * @param \WC_Order $order
 */
function clover_wc_process_order_meta_box_action( $order ) {
    
	//Do Clover stuff
	// var_dump($order);
	// var_dump($order->get_items());


	//building order xml;
	$xmlOrder = '<?xml version="1.0" encoding="utf-8"?>';
	$xmlOrder .= '<OrderSubmit>
				<Credential>
					<UserID>CUST-143363</UserID>
					<CompanyCd>01</CompanyCd>
					<SenderID>northwoodsprinters</SenderID>
					<Password>!northwoodsprinters!</Password>
				</Credential>';
	$xmlOrder .= 	"<PODate>".date('n/j/Y')."</PODate>
					<RequiredDate>".date('n/j/Y')."</RequiredDate>";				


	$xmlOrder .= '<Payment>
					<Terms>Credit Card</Terms>
					</Payment>';

	var_dump($order->get_address('billing'));
	var_dump($order->get_address('shipping'));

	$billingAddress = $order->get_address('billing');
	$shippingAddress = $order->get_address('shipping');
	foreach ($shippingAddress as $key => $value) {
	 	if ($value === ''){
	 		$shippingAddress[$key] = $billingAddress[$key];
	 	}
	 } 

	 // if ($billingAddress['first_name'] !== '' &&
	 // 	 $billingAddress['last_name'] !== '' &&
	 // 	 $billingAddress['company'] !== '' 
	 // 	 ){
	 // 	$billingAddress['attn'] = $billingAddress['first_name'].' '.$billingAddress['last_name'];
	 // 	$billingAddress['name'] = $billingAddress['company'];
 	//  }
 	//  else {
 	//  	$billingAddress['name'] = $billingAddress['first_name'].' '.$billingAddress['last_name'];
	 // 	$billingAddress['attn'] = '';
 	//  }

 	 if ($shippingAddress['first_name'] !== '' &&
	 	 $shippingAddress['last_name'] !== '' &&
	 	 $shippingAddress['company'] !== '' 
	 	 ){
	 	$shippingAddress['attn'] = $shippingAddress['first_name'].' '.$shippingAddress['last_name'];
	 	$shippingAddress['name'] = $shippingAddress['company'];
 	 }
 	 else {
 	 	$shippingAddress['name'] = $shippingAddress['first_name'].' '.$shippingAddress['last_name'];
	 	$shippingAddress['attn'] = '';
 	 }

	$xmlOrder .= '<!--Billing Information-->
					<BillTo>
						<Address>
							<Name>Northwoods Office Express</Name>
							<Addr1>3311 S. Airport Rd West</Addr1>
							<City>Traverse City</City>
							<State>MI</State>
							<Zip>49684</Zip>
						</Address>
						<EmailFlag>1</EmailFlag><!--Optional-->
						<EmailAddr>info@northwoodsprinters.com</EmailAddr><!--Optional-->
						<PONumber>'.$order->get_id().'</PONumber>
						<Id>'.$order->get_id().'</Id><!--Optional-->
					</BillTo>
					<ShipTo>
						<Address>
							<Name>'.$shippingAddress['name'].'</Name>
							<Addr1>'.$shippingAddress['address_1'].'</Addr1>
							<Addr2>'.$shippingAddress['address_2'].'</Addr2> <!--Optional-->
							<City>'.$shippingAddress['city'].'</City>
							<State>'.$shippingAddress['state'].'</State>
							<Zip>'.$shippingAddress['postcode'].'</Zip>
							<Attn>'.$shippingAddress['attn'].'</Attn><!--Optional-->
						</Address>
						<PONumber>'.$order->get_id().'</PONumber>
						<Id>'.$order->get_id().'</Id><!--Optional-->
					</ShipTo>';


	$xmlOrder .= '<ItemList>';

	$orderItems = $order->get_items();
	foreach ($orderItems as $key => $value) {
		//if this is a clover product
		if( get_post_meta( $value->get_product()->get_id(), '_cig_product', true ) !== ''){
			
			$xmlOrder .= '<Item>
			<SKU>'. $value->get_product()->get_sku() .'</SKU>
			<Qty>'. $value->get_quantity().'</Qty>
			<Price>'. $value->get_product()->get_price() .'</Price>
			<Uom>EA</Uom>
			</Item>';
			//var_dump (  );
		}	
	}

	$xmlOrder .= '</ItemList>';


	$xmlOrder .= '<Notes>Any Special notes or instructions</Notes><!--Optional-->
</OrderSubmit>';
	var_dump($xmlOrder);

	$handle = fopen( plugin_dir_path(__file__ )."orderLogs/order-".$order->get_id()."-".date("Y-m-d").".xml", 'w');
	fwrite($handle, $xmlOrder);
	fclose($handle);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,'http://www.westpointproducts.com/ci/CustomerIntegration.aspx');
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlOrder);
	$response = curl_exec($ch);
	
	$handle = fopen( plugin_dir_path(__file__ )."orderLogs/order-".$order->get_id()."-".date("Y-m-d")."-response.xml", 'w');
	fwrite($handle, $response);
	fclose($handle);
	var_dump($response);



	wp_mail( 'james@leadplanmarketing.com', 'CloverDropShip-'.$order->get_id(), $xmlOrder.$response);

    // add the order note
    // translators: Placeholders: %s is a user's display name
    $message = sprintf( __( 'Order Drop Shipped Via Clover | ', 'my-textdomain' ), wp_get_current_user()->display_name );
    $order->add_order_note( $message );
    
    $order->update_status('completed', $message);
    
    //wp_die();
    // add the flag
    update_post_meta( $order->id, '_wc_order_clover_drop_shipped', 'yes' );
}
add_action( 'woocommerce_order_action_wc_clover_drop_ship', 'clover_wc_process_order_meta_box_action' );



?>